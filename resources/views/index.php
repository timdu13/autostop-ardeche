<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Application Autostop Ardèche</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- build:css(.tmp) styles/vendor.css -->
        <!-- bower:css -->
        <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.css" />
        <!-- endbower -->
        <!-- endbuild -->
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.44.0/mapbox-gl.css' rel='stylesheet' />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        
        <!-- build:css(.tmp) styles/main.css -->
        <link rel="stylesheet" href="app/styles/main.css">
        <!-- endbuild -->
    </head>
    <body ng-app="autostopApp">
        
        <header ng-controller="NavigationController">
            <ng-include src="'app/views/nav.html'"></ng-include>
        </header> 


        <div class="container-fluid container-autostopApp">
            <div ng-view></div>
        </div>

        <footer>
            <ng-include src="'app/views/footer.html'"></ng-include>
        </footer>

        <!-- Mapbox -->
        <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.44.0/mapbox-gl.js'></script>
        <script>
            mapboxgl.accessToken = 'pk.eyJ1IjoidGltYWp0IiwiYSI6ImNqYWsxdDFwNDN4MnUzM251Z282dnRyOXMifQ.gprz6u6FDQkrZaWLIG0A8g';
        </script>

        <!-- build:js(.) scripts/vendor.js -->
        <!-- bower:js -->
        <script src="bower_components/jquery/dist/jquery.js"></script>
        <script src="bower_components/angular/angular.js"></script>
        <script src="bower_components/bootstrap/dist/js/bootstrap.js"></script>
        <script src="bower_components/angular-route/angular-route.js"></script>
        <script src="bower_components/angular-animate/angular-animate.js"></script>
        <script src="bower_components/angular-bootstrap/ui-bootstrap-tpls.js"></script>
        <script src="bower_components/angular-cookies/angular-cookies.js"></script>
        <script src="bower_components/angular-sanitize/angular-sanitize.js"></script>
        <!-- endbower -->
        <!-- endbuild -->

        <!-- build:js({.tmp,app}) scripts/scripts.js -->
        <script src="app/scripts/app.js"></script>

        <script src="app/scripts/controllers/navigation.js"></script>
        <script src="app/scripts/controllers/main.js"></script>
        <script src="app/scripts/controllers/postJourney.js"></script>
        <script src="app/scripts/controllers/inscription.js"></script>
        <script src="app/scripts/controllers/resultats.js"></script>
        <script src="app/scripts/controllers/messagerie.js"></script>
        <script src="app/scripts/controllers/discussion.js"></script>
        <script src="app/scripts/controllers/contact.js"></script>
        <script src="app/scripts/controllers/mesTrajets.js"></script>


        <script src="app/scripts/services/authentication.js"></script>
        <script src="app/scripts/services/mapbox_api.js"></script>
        <script src="app/scripts/services/autostop_api.js"></script>
        <!-- endbuild -->
    </body>
</html>
