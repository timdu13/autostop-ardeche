'use strict';

angular.module( 'autostopApp' )
.service('AutostopAPIService', [
    '$http',
    '$q',
    function( $http, $q ) {


        this.newJourney = function ( journey ) {
            var req = {
                method: 'POST',
                url: 'api/journey/new',
                xhrFields: {
                    withCredentials: true
                },
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'authorization' : 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjNkMjI5MzVkODlmNDI0ZTUwNTJhODFjZDFhNDIwYjE1MjZlYTQzYTIwYTg1MGIxYzNmZjcwM2RiNjk5MDgwYzU5NGU5MDkwYTU0MGE3NjI0In0.eyJhdWQiOiIxIiwianRpIjoiM2QyMjkzNWQ4OWY0MjRlNTA1MmE4MWNkMWE0MjBiMTUyNmVhNDNhMjBhODUwYjFjM2ZmNzAzZGI2OTkwODBjNTk0ZTkwOTBhNTQwYTc2MjQiLCJpYXQiOjE1MTgzNjc5NzEsIm5iZiI6MTUxODM2Nzk3MSwiZXhwIjoxNTQ5OTAzOTcxLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.UhGd0uYN7CzBdrxL25mNsBWYAVskdY1DwCx-nF-NMkHHFw8-aYDFz5bHKO0fZIDhkzb33S41r6o7dvUek8_m7UxXMlcF0G7vpC9IUbBnvvFMxkeGckLppfyDSzYGThPnSYXpD5gPtayvJxZfMra6G72izKZO6RXwpGIbI0lWkq_zgIKUC-X-xto9cQueFpdentp6TvJUh7iXucwCf0vHwksDmQ27vcrn7wNKBEwhaEvYbLNIeqIcGv1z3rHkDcTKFZe9zfXN3Q9SngLJ8dkR3gUQQbCOXvTYLKU-dzurSsr_ixZnu5A6hJ1jM9LrZm7qQGCJQ0B4nqotAbZ55AB9OdZfGZMgwb57egYVigeQUrBg-23la677OOj_4Uqw0jMDbdpSdPBoUyc3D_12NCRaTKg71L9fBRxtU9-sNgKQQGNrkNbyjqTHRY7WMBANegZtCPRhI21uKqEi6RFcG7IQzXjwuqUb-oTk00IFHZjqUUvsNHbnUmYVPHzoNeqsVcyj75cQPtPfKtXi3Nl8byFWlTZHu8ei9Peg_yUln7heu2GMJq0wH1-djIUb9Jk7y7ALzLLhU76OypCEB-eNHyqAyCRhhEGxIrumNV_GCG04zi4D7hH2s7jI9NrGwWSy0DLZi_-FJjKBQGRX5IiWCkwO4XFwYuyrcjBjOiAw5ATaEXU',
                },
                data : {
                    'user_id': journey.user_id,
                    'type': journey.type,
                    'address_start': journey.address_start,
                    'address_end': journey.address_end,
                    'date_start': journey.date_start,
                }
            };

            $http( req ).then(function( response ){
                console.log( response );
            });
        };
        
        
    }
]);